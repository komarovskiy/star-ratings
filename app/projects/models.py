from statistics import mean


from django.db import models
from django.conf import settings
from django.contrib.contenttypes.fields import GenericRelation


class Project(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL, related_name='projects', on_delete=models.CASCADE)
    title = models.CharField(max_length=255)
    description = models.TextField()
    votes = GenericRelation('ratings.Vote')

    def __str__(self):
        return self.title

    def get_rating(self):
        votes_data = self.votes.values_list('value', flat=True)
        if len(votes_data) > 0:
            return round(mean(votes_data))
        return 0
