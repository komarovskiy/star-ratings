# -*- coding: utf-8 -*-

from braces.views._access import AccessMixin
from django.http import Http404


from .models import Project


class OwnerRequiredMixin(AccessMixin):

    def dispatch(self, request, *args, **kwargs):
        if not request.user.is_authenticated:
            return self.handle_no_permission(request)
        try:
            project = Project.objects.get(pk=kwargs['pk'], user=request.user)
        except Project.DoesNotExist:
            raise Http404()
        return super(OwnerRequiredMixin, self).dispatch(request, *args, **kwargs)
