from django.views.generic import CreateView, UpdateView, DeleteView, ListView, DetailView
from django.urls import reverse


from braces.views import LoginRequiredMixin


from .models import Project
from .mixins import OwnerRequiredMixin
from .tables import OwnProjectsTable
from .forms import UserProjectManageForm


class UserProjectsView(ListView):
    template_name = 'projects_list.html'
    model = Project
    context_object_name = 'projects'

    def get_queryset(self):
        return OwnProjectsTable(self.request.user.projects.all())


class UserProjectsCreateView(LoginRequiredMixin, CreateView):
    template_name = 'projects_form.html'
    model = Project
    form_class = UserProjectManageForm

    def form_valid(self, form):
        self.object = form.save(commit=False)
        self.object.user = self.request.user
        self.object.save()
        return super(UserProjectsCreateView, self).form_valid(form)

    def get_success_url(self):
        return reverse('own_projects')


class UserProjectUpdateView(OwnerRequiredMixin, UpdateView):
    template_name = 'projects_form.html'
    model = Project
    form_class = UserProjectManageForm

    def get_success_url(self):
        return reverse('own_projects')


class UserProjectDeleteView(OwnerRequiredMixin, DeleteView):
    template_name = 'projects_confirm_delete.html'
    model = Project

    def get_success_url(self):
        return reverse('own_projects')


class ProjectDetailView(DetailView):
    template_name = 'projects_details.html'
    context_object_name = 'project'
    model = Project
