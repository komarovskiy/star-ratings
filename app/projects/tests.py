from django.test import TestCase
from django.urls import reverse

from accounts.models import Account
from .models import Project


class ProjectTest(TestCase):
    fixtures = ['app']

    def setUp(self):
        self.user = Account.objects.get(pk=1)
        project = Project()
        project.user = self.user
        project.title = 'Superbowl project'
        project.description = 'Join Superbowl supporters project!'
        project.save()
        self.project = project
        self.client.login(username=self.user.email, password="Panda#123")

    def test_user_projects_model(self):
        self.assertEqual(self.user.projects.count(), 11)
        self.assertEqual(self.project.get_rating(), 0)

    def test_project_add_view(self):
        response = self.client.post(reverse('add_project'),
                                    {
                                        "title": "NBA finals project",
                                        "description": "Glory to GSW!"
                                    })
        self.assertEqual(response.status_code, 302)
        self.assertEqual(self.user.projects.count(), 12)

    def test_project_update_view(self):
        response = self.client.post(reverse('edit_project', args=[self.project.pk]),
                                    {
                                        "title": "NBA finals project",
                                        "description": "Glory to GSW!"
                                    })
        project = Project.objects.get(pk=self.project.pk)
        self.assertEqual(response.status_code, 302)
        self.assertEqual(project.title, "NBA finals project")
        self.assertEqual(project.description, "Glory to GSW!")
        self.assertEqual(self.user.projects.count(), 11)

    def test_project_delete_view(self):
        response = self.client.post(reverse('delete_project', args=[self.project.pk]))
        self.assertEqual(response.status_code, 302)
        self.assertEqual(self.user.projects.count(), 10)


