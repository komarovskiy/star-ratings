from django.views.generic import TemplateView, View
from django.http.response import HttpResponseBadRequest, JsonResponse
from django.contrib.contenttypes.models import ContentType
from django.views.decorators.csrf import csrf_exempt


from django.db.models import Avg, IntegerField
from accounts.models import Account
from projects.models import Project
from accounts.tables import AccountTable
from projects.tables import ProjectTable


from .models import Vote


class RatingsView(TemplateView):
    template_name = 'ratings.html'

    def reorder(self, order):
        if order.startswith('-'):
           return 'rating'
        return '-rating'

    def get_context_data(self, **kwargs):
        accounts_order = self.request.session.get('accounts_order', '-rating')
        projects_order = self.request.session.get('projects_order', '-rating')

        self.request.session['accounts_order'] = accounts_order
        self.request.session['projects_order'] = projects_order

        if self.request.GET.get('accounts-sort') == 'rating':
            accounts_order = self.reorder(accounts_order)

        if self.request.GET.get('projects-sort') == 'rating':
            projects_order = self.reorder(projects_order)

        context = super(RatingsView, self).get_context_data(**kwargs)
        accounts_table = AccountTable(Account.objects.annotate(rating=Avg('votes__value',
                                                                          output_field=IntegerField())
                                                               ).order_by(accounts_order), prefix='accounts-')
        projects_table = ProjectTable(Project.objects.annotate(rating=Avg('votes__value',
                                                                          output_field=IntegerField())
                                                               ).order_by(projects_order), prefix='projects-')

        context['accounts'] = accounts_table
        context['projects'] = projects_table
        return context


class VoteView(View):
    @csrf_exempt
    def dispatch(self, request, *args, **kwargs):
        return super(VoteView, self).dispatch(request, *args, **kwargs)

    def post(self, request, **kwargs):
        if not request.user.is_authenticated:
            has_voted = request.session.get('anonymous_vote')
            if has_voted == -1:
                request.session['anonymous_vote'] = 0
            else:
                return JsonResponse({
                    'message': 'You can`t vote more than one time as anonymous. Please register',
                    'tokens_left': True
                })
        content_type = kwargs.get('slug')
        object_pk = kwargs.get('pk')
        vote = Vote()
        try:
            vote.value = int(request.POST.get('value'))
        except:
            return HttpResponseBadRequest()
        vote.object_id = object_pk
        vote.content_type = ContentType.objects.get(model=content_type)
        vote.save()

        if request.user.is_authenticated:
            if request.user.get_tokens_count() > 0:
                token = request.user.tokens.filter(is_used=False).first()
                token.is_used = True
                token.save()
                return JsonResponse({
                    'message': 'You`ve voted succesfully!',
                    'tokens_amount': request.user.get_tokens_count()
                })
            else:
                return JsonResponse({
                    'message': 'You don`t have tokens to vote!',
                    'tokens_left': True
                })
        return JsonResponse({
            'message': 'You`ve voted succesfully! Login or register to have more attempts.',
            'tokens_left': True
        })
