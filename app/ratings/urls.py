# -*- coding: utf-8 -*-
from django.urls import path
from django.views.decorators.cache import cache_page


from .views import RatingsView, VoteView


urlpatterns = [
    path('', cache_page(60*15)(RatingsView.as_view()), name='ratings_page'),
    #path('', RatingsView.as_view(), name='ratings_page'),
    path('vote_<str:slug>/<int:pk>/', VoteView.as_view(), name='vote')
]
