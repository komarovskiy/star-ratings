import json
from django.test import TestCase
from django.urls import reverse

from accounts.models import Account


class ProjectTest(TestCase):
    fixtures = ['app']

    def setUp(self):
        self.user = Account.objects.get(pk=1)
        self.new_user = Account.objects.create_user(full_name='Zero', email='zero@zeromail.com', password='Zero#123',
                                                    is_active=True)

    def test_current_user_rating(self):
        self.assertEqual(self.user.get_rating(), 3)

    def test_new_user_rating(self):
        self.assertEqual(self.new_user.get_rating(), 0)

    def test_voting_endpoint_vote_for_user_anonymous(self):
        response = self.client.post(reverse('vote', args=['account', 2]), {'value': 5})

        self.assertEqual(response.status_code, 200)
        content = json.loads(response.content)
        self.assertEqual(content['message'],
                         "You`ve voted succesfully! Login or register to have more attempts.")
        self.assertTrue(content['tokens_left'])

    def test_voting_endpoint_vote_for_user_logged_in(self):
        self.client.login(username=self.user.email, password="Panda#123")
        self.assertEqual(self.user.get_tokens_count(), 15)
        response = self.client.post(reverse('vote', args=['account', 2]), {'value': 5})
        self.assertEqual(self.user.get_tokens_count(), 14)
        content = json.loads(response.content)
        self.assertEqual(content['message'], 'You`ve voted succesfully!')
