# -*- coding: utf-8 -*-

from django.utils.deprecation import MiddlewareMixin


class AnonymousVotesMiddleware(MiddlewareMixin):

    def process_request(self, request):
        if not request.user.is_authenticated and request.session.get('anonymous_vote', None) is None:
            request.session['anonymous_vote'] = -1
