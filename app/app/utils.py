# -*- coding: utf-8 -*-

from django.utils.html import format_html
from django.template.loader import render_to_string
from django.urls import reverse


import django_tables2 as tables


class VoteColumn(tables.Column):
    def __init__(self, *args, **kwargs):
        self.object_type = kwargs.pop('object_type')
        super(VoteColumn, self).__init__(*args, **kwargs)

    def render(self, value):
        return format_html(render_to_string(
            "utils/star-ratings-widget.html",
            context={'object_type': self.object_type, 'value': value}
            )
        )


class ActionColumn(tables.Column):
    def __init__(self, **kwargs):
        self.action = kwargs.pop('action')
        self.label = self.action.capitalize()
        super(ActionColumn, self).__init__(**kwargs)

    def render(self, value):
        return format_html("<a href='{}'>{}</a>".format(
            reverse('{}_project'.format(self.action), kwargs={'pk': value}), self.label)
        )
