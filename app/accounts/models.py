import uuid
from statistics import mean


from django.db.models.signals import post_save
from django.dispatch import receiver
from django.db import models
from django.contrib.auth.models import AbstractBaseUser, PermissionsMixin
from django.contrib.contenttypes.fields import GenericRelation


from imagekit.models import ImageSpecField
from imagekit.processors import ResizeToFill


from .managers import AccountManager


class Account(AbstractBaseUser, PermissionsMixin):
    full_name = models.CharField(max_length=255)
    email = models.EmailField(max_length=255, unique=True)
    created_at = models.DateTimeField(auto_now_add=True)
    is_active = models.BooleanField(default=True)
    is_staff = models.BooleanField(default=True)
    is_admin = models.BooleanField(default=False)

    votes = GenericRelation('ratings.Vote')

    objects = AccountManager()

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['full_name']

    def __str__(self):
        return self.email

    def get_rating(self):
        votes_data = self.votes.values_list('value', flat=True)
        if len(votes_data) > 0:
            return round(mean(votes_data))
        return 0

    def get_tokens_count(self):
        return self.tokens.filter(is_used=False).count()


class AccountVoteToken(models.Model):
    account = models.ForeignKey(Account, related_name='tokens', on_delete=models.CASCADE)
    value = models.CharField(max_length=255)
    is_used = models.BooleanField(default=False)


@receiver(post_save, sender=Account)
def generate_tokens(sender, **kwargs):
    if kwargs.get('created'):
        instance = kwargs.get('instance')
        AccountVoteToken.objects.bulk_create([
            AccountVoteToken(account=instance, value="{}{}".format(x, uuid.uuid4().hex)) for x in range(15)
        ])
