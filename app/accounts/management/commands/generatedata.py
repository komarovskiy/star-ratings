# -*- coding: utf-8 -*-
import uuid
import random


from django.core.management.base import BaseCommand
from django.contrib.contenttypes.models import ContentType


from accounts.models import Account, AccountVoteToken
from projects.models import Project
from ratings.models import Vote


class Command(BaseCommand):
    help = """
    Generates test data.
    All users has passwords Panda#123
    """

    def handle(self, *args, **options):
        for id in range(15):
            account = Account.objects.create_user(
                full_name='User #{}'.format(id),
                email="user{}@starmail.com".format(id),
                password='Panda#123',
                is_active=True,
                is_admin=False
            )

            Project.objects.bulk_create([
                Project(user=account, title='{} story #{}'.format(account.full_name, x)) for x in range(10)
            ])

            Vote.objects.bulk_create([
                Vote(value=random.choice(range(1, 6)), content_type=ContentType.objects.get(model='account'),
                     object_id=account.pk) for i in range(25)
            ])

        for project in Project.objects.all():
            Vote(value=random.choice(range(1, 6)), content_type=ContentType.objects.get(model='project'),
                 object_id=project.pk).save()
