# -*- coding: utf-8 -*-
from django.urls import path
from django.contrib.auth.views import logout
from .views import RegistrationView, HomePageView

urlpatterns = [
    path('login/', HomePageView.as_view(), kwargs={'next_page': '/'}, name='login'),
    path('logout/', logout, kwargs={'next_page': '/'}, name='logout'),
    path('register/', RegistrationView.as_view(), name='register'),
]