from django import forms
from django.contrib.auth.forms import AuthenticationForm

from .models import Account


class UserCreationForm(forms.ModelForm):
    password1 = forms.CharField(label="Password", widget=forms.PasswordInput)
    password2 = forms.CharField(label="Confirm password", widget=forms.PasswordInput)

    def clean_password2(self):
        password1 = self.cleaned_data.get('password1')
        password2 = self.cleaned_data.get('password2')
        if password1 and password2 and password1 != password2:
            raise forms.ValidationError('Passwords don`t match')
        return password2

    def save(self, commit=True):
        user = super(UserCreationForm, self).save(commit=False)
        user.set_password(self.cleaned_data['password1'])
        if commit:
            user.save()
        return user

    class Meta:
        model = Account
        fields = ('full_name', 'email', )


class RegistrationForm(UserCreationForm):
    full_name = forms.CharField(label='Full Name', max_length=255,
        widget=forms.TextInput(attrs={'class': "form-control"}))
    email = forms.EmailField(label='Email', max_length=75,
        widget=forms.EmailInput(attrs={'class': "form-control"}))
    password1 = forms.CharField(min_length=6, max_length=16, label="Password",
        widget=forms.PasswordInput(attrs={'class': "form-control"}))
    password2 = forms.CharField(min_length=6, max_length=16, label="Confirm Password",
        widget=forms.PasswordInput(attrs={'class': "form-control"}),
        )

    class Meta:
        model = Account
        fields = ('full_name', 'email', 'password1', 'password2')

    def clean_email(self):

        email = self.cleaned_data["email"]
        try:
            user = Account.objects.get(email=email)
            raise forms.ValidationError("User with this email already exists.")
        except Account.DoesNotExist:
            return email

    def save(self, commit=True):
        user = super(UserCreationForm, self).save(commit=False)
        user.set_password(self.cleaned_data["password1"])
        user.email = self.cleaned_data["email"]
        user.is_active = False
        if commit:
            user.save()
        return user


class LoginForm(AuthenticationForm):
    username = forms.EmailField(label='Email', max_length=75,
                             widget=forms.EmailInput(attrs={'class': "form-control"}))
    password = forms.CharField(min_length=6, max_length=16, label="Password",
                               widget=forms.PasswordInput(attrs={'class': "form-control"}))

