# -*- coding: utf-8 -*-
import django_tables2 as tables
from app.utils import VoteColumn

from .models import Account


class AccountTable(tables.Table):
    full_name = tables.Column(orderable=False)
    vote = VoteColumn(verbose_name='Vote Now!', accessor='id', orderable=False, object_type='account')

    class Meta:
        model = Account
        template_name = 'django_tables2/bootstrap.html'
        fields = ('full_name', 'rating')
